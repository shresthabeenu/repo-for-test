class Login{
    
    visit(){

        cy.visit('http://wallbee.codebeestaging.com/')
    }

    username(){
        return cy.get('#UserName')
    }
    password(){
        return cy.get('#Password')
    }
    loginbtn(){
        return cy.get('.btn').click()
    }
    
}

export default Login

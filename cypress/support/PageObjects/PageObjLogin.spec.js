/// <reference types = "Cypress"/>
import LoginPage from '../../integration/StdSysTests/LoginPage'

describe('Login',()=>{
    it('Login to shipment page',()=>{

        const lp = new LoginPage()
        lp.visit()
        lp.fillusername('wallbeeadmin')
        lp.fillpassword('Wallbee123!')
        lp.submit()
        cy.title().should('be.equal','WALLBEE - Create Shipment')

    })


    
})
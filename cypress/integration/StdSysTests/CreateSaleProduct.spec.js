describe('Create sale product',()=>{
    before('Login',()=>{

        cy.Login('wallbeeadmin','202!@Wall83e!')
        cy.title().should('be.equal','WALLBEE - Create Shipment')
    
    })

    beforeEach('Redirect to the sale product index page',()=>{

        cy.visit(Cypress.env('salesproduct'))
        
    })

    it ('Create sale product',()=>{
        
        //Click on create button
        cy.get('.page-title > .btn').click()
        cy.wait(10000)
        //Enter Internal name
        cy.get('#InternalName').type('Saleproduct')
        //Enter display name
        cy.get('#DisplayName').type('saleproduct')
        //Enter producftid
        cy.get('#ProductTypeID').select('COURIER')
        //Click on the drop down and select company type
       // cy.get('#select2-TransportCompanyId-container').invoke('show')
        cy.get('span.selection>span.select2-selection--single>span#select2-TransportCompanyId-container').click()
        //Click on the drop down and select owner country
        cy.get('#OwnerCountry').select('DK')
        //Enter insurance
        cy.get('#Insurances').type('1000')
        //Enter salenumber
        cy.get('#SaleNumber').type('1000')
        //Enter max weight
        cy.get('#MaxWeights').type('1000')
        //Enter max vol weight
        cy.get('#MaxVolWeights').type('1000')
        //Enter max colli weight
        cy.get('#MaxColliWeights').type('1000')
        //Enter max colli vol weight
        cy.get('#MaxColliVolWeights').type('1000')
        //
        //Click on the Create button
        cy.get('#btnSave').click()
        cy.title().should('be.equal','WALLBEE - Sale Products')

})

})

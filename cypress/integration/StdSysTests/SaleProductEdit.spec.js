describe('Edit sale product',()=>{
    before('Login',()=>{

        cy.Login('wallbeeadmin','202!@Wall83e!')
        cy.title().should('be.equal','WALLBEE - Create Shipment')
    
    })

    beforeEach('Redirect to the sale product index page',()=>{

        cy.visit(Cypress.env('salesproduct'))
        
    })

    it ('Create sale product',()=>{

        //Click on the dit button
        cy.get(':nth-child(1) > #Links > .action-list > :nth-child(3) > .actions-btn > .fa').click()
        cy.wait(10000)
        //Edit Internal name
        cy.get('#InternalName').type('Saleproduct')
        //Edit display name
        cy.get('#DisplayName').type('saleproduct')
        //Update insurance
        cy.get('#Insurances').type('1000')
        //Update salenumber
        cy.get('#SaleNumber').type('1000')
        //Update max weight
        cy.get('#MaxWeights').type('1000')
        //Update max vol weight
        cy.get('#MaxVolWeights').type('1000')
        //Update max colli weight
        cy.get('#MaxColliWeights').type('1000')
        //Update max colli vol weight
        cy.get('#MaxColliVolWeights').type('1000')
        //Click on the Save  button
        cy.get('#btnSave').click()
        cy.title().should('be.equal','WALLBEE - Sale Products')

})

})

/// <reference types = "Cypress"/>
import LoginPage from '../StdSysTests/LoginPage'


describe('select products from account page',()=>{
    before('Login to shipment page',()=>{

        const login = new LoginPage()
        login.visit()
        login.fillusername('wallbeeadmin')
        login.fillpassword('19Wallb3e!')
        login.submit()
        cy.title().should('be.equal','LEMAN - Create Shipment')


    })

    beforeEach('Redirect to the transport product page',()=>{

        cy.get('.ui-dialog-buttons > .ui-dialog-titlebar > .ui-dialog-titlebar-close').click()
        cy.visit(Cypress.env('transport_product'))
        cy.wait(1000)
        //cy.get('div.position-fix:nth-child(1) header.head nav.navi div.container-fluid ul.big-nav li.dropdown:nth-child(6) > ul.dropdown-menu').invoke('show')
        //cy.get('.SalesProduct').click()
        
    })

    it ('Create sale product',()=>{
        cy.get('.page-title > .btn').click()
        cy.get('#InternalName').type('Asendia_EPAQ_ELT_SIG_EL45')
        cy.get('#DisplayName').type('Asendia_EPAQ_ELT_SIG_EL45')
        cy.get('#CompanyTypeID').select('Asendia')
        //cy.get(':nth-child(3) > .switch-button-background > .switch-button-button').click()
        cy.get('#CurrencyCode').select('Danish Krone')
        cy.get('#TransportNumber').clear().type('1000')

        //cy.get('#WallServiceProductID').select('Asendia_EPAQ_STD_PR')
          //confusedcy.get('#WallServiceProductID').select('Asendia_EPAQ_SCT_PUDO_EL150')
        //cy.get('#WallServiceProductID').select('Asendia_EPAQ_PLS_MBD_EL45')
        //cy.get('#WallServiceProductID').select('Asendia_EPAQ_PLS_PLD_EL150')
        //cy.get('#WallServiceProductID').select('Asendia_EPAQ_PLS_PLD_EL45')
        //cy.get('#WallServiceProductID').select('Asendia_EPAQ_SCT_EL500')
        //cy.get('#WallServiceProductID').select('Asendia_EPAQ_SCT_EL150')
       // cy.get('#WallServiceProductID').select('Asendia_EPAQ_SCT_EL45')
        //cy.get('#WallServiceProductID').select('Asendia_EPAQ_SCT_SIG_EL500')
        // cy.get('#WallServiceProductID').select('Asendia_EPAQ_SCT_SIG_EL150')
       // cy.get('#WallServiceProductID').select('Asendia_EPAQ_SCT_SIG_EL45')
        //cy.get('#WallServiceProductID').select('Asendia_EPAQ_SCT_PUDO_EL500')
        //cy.get('#WallServiceProductID').select('Asendia_EPAQ_SCT_PUDO_EL150')
        //cy.get('#WallServiceProductID').select('Asendia_EPAQ_SCT_PUDO_EL45')
        //cy.get('#WallServiceProductID').select('Asendia_EPAQ_ELT_EL500')
        //cy.get('#WallServiceProductID').select('Asendia_EPAQ_ELT_EL150')
        //cy.get('#WallServiceProductID').select('Asendia_EPAQ_ELT_EL45')
         //cy.get('#WallServiceProductID').select('Asendia_EPAQ_ELT_SIG_EL500')
        //cy.get('#WallServiceProductID').select('Asendia_EPAQ_ELT_SIG_EL150')
        cy.get('#WallServiceProductID').select('Asendia_EPAQ_ELT_SIG_EL45')
        cy.get('#MaxWeights').clear().type('1000')
        cy.get('#MaxVolWeights').clear().type('1000')
        cy.get('#MaxColliWeights').clear().type('1000')
        cy.get('#MaxColliVolWeights').clear().type('1000')
        cy.get('#MaxHeight').clear().type('1000')
        cy.get('#MaxWidth').clear().type('1000')
        cy.get('#MaxLength').clear().type('1000')
        cy.get('#MaxGrid').clear().type('1000')
        cy.get('#InvoiceMargin').clear().type('1000')
        cy.get('.btn').click()
        cy.title().should('be.equal','LEMAN - Transport Products')

})

})
